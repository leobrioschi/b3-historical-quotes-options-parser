import numpy as np
import pandas as pd
import pyarrow as pa
import pyarrow.csv
from tqdm import tqdm
import datetime,os,zipfile,re,io,requests

# https://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A1991.zip

class b3_historical:
    ''' An object that reads raw Historical Quotes from
    b3 and stores in a pandas dataframe.
    --------------------------------------------------- 
    variable = b3_historical('source path')
        initializes the object.
        
    Methods:
    - neg_code(self, code=None/'str', inplace=False/True)
    - bdi_code(self, code=None/'str', inplace=False/True)
    - specification(self, code=None/'str', inplace=False/True)
    - options(code=None/'str', company_code=None/'str', type=None/'Put'/'Call' ,inplace=False/True)
    - index_options(code=None/'str', index_code=None/'str', type=None/'Put'/'Call' ,inplace=False/True)
    - to_csv(self, data=None, path='b3_historical.csv')
    - to_pandas(self, data=None)
    '''


    def __init__(self, source_path):
        # Checking if years is a list or a integer
        if isinstance(source_path, str):
            source_path = [source_path]
        elif not isinstance(source_path, list):
            raise ValueError(f'Function needs a source path or list of paths format YYYY, got {source_path}')
        self.source = source_path
        self.data = self.data_read()

    def neg_code(self, code=None, inplace=False):
        '''Filters the historical data for quote code
        ----------------------------------------------
        neg_code(self, code=None/'str', inplace=False/True)
        '''
        if code == None:
            raise ValueError(f'No quote code provided for the filter')
        else:
            table_filter = self.data
            mask = pa.compute.starts_with(table_filter['NEG CODE'], code)
            table_filter = table_filter.filter(mask)
            if inplace == False:
                return table_filter
            else:
                self.data = table_filter
                return self.data

    def bdi_code(self, code=None, inplace=False):
        '''Filters the historical data for bdi code
        ----------------------------------------------
        bdi_code(self, code=None/'str', inplace=False/True)
        '''
        if code == None:
            raise ValueError(f'No BDI code provided for the filter')
        else:
            table_filter = self.data
            mask = pa.compute.starts_with(table_filter['BDI CODE'], code)
            table_filter = table_filter.filter(mask)
            if inplace == False:
                return table_filter
            else:
                self.data = table_filter
                return self.data

    def specification(self, code=None, inplace=False):
        '''Filters the historical data for specification code [ESPECI TABLE]
        ----------------------------------------------
        specification(self, code=None/'str', inplace=False/True)
        '''
        if code == None:
            raise ValueError(f'No specification code provided for the filter')
        else:
            table_filter = self.data
            mask = pa.compute.starts_with(table_filter['SPEC'], code)
            table_filter = table_filter.filter(mask)
            if inplace == False:
                return table_filter
            else:
                self.data = table_filter
                return self.data


    def options(self, code=None, company_code=None, type=None ,inplace=False):
        ''' Return an Arrow Table only with options
        options(code=None/'str', company_code=None/'str', type=None/'Put'/'Call' ,inplace=False/True)
        ------------------------------------------
        **code**(None is default): 
        If no option code is provided, 
        returns all equity options.
        **company_code**(None is default): 
        Searches for 4 letter Company Code.
        **inplace**(False is default):
        Replaces the object's data with resulting dataset.
        **type**(None is default):
        Filters by Call or Put.'''
        
        call_code = '78'
        put_code = '82'
        filter_code = False

        table_filter = self.data
        
        if type == 'Call':
            mask = pa.compute.equal(table_filter['BDI CODE'], call_code)
            table_filter = table_filter.filter(mask) 
        elif type == 'Put':
            mask = pa.compute.equal(table_filter['BDI CODE'], put_code)
            table_filter = table_filter.filter(mask) 
        else:
            mask1 = pa.compute.equal(table_filter['BDI CODE'], call_code)
            mask2 = pa.compute.equal(table_filter['BDI CODE'], put_code)
            mask = pa.compute.or_(mask1,mask2)
            table_filter = table_filter.filter(mask) 
        
        if company_code != None:
            mask = pa.compute.starts_with(table_filter['NEG CODE'], company_code)
            table_filter = table_filter.filter(mask) 

        if code != None:
            mask = pa.compute.equal(table_filter['NEG CODE'], code)
            table_filter = table_filter.filter(mask) 

        if inplace == True:
            self.data = table_filter
            return self
        else:
            return table_filter

    def index_options(self, code=None, index_code=None, type=None ,inplace=False):
        ''' Return an Arrow Table only with options
        index_options(code=None/'str', index_code=None/'str', type=None/'Put'/'Call' ,inplace=False/True)
        ------------------------------------------
        **code**(None is default): 
        If no option code is provided, 
        returns all equity options.
        **index_code**(None is default): 
        Searches for 4 letter Index Code.
        **inplace**(False is default):
        Replaces the object's data with resulting dataset.
        **type**(None is default):
        Filters by Call or Put.'''
        
        call_code = '74'
        put_code = '75'
        filter_code = False

        table_filter = self.data
        
        if type == 'Call':
            mask = pa.compute.equal(table_filter['BDI CODE'], call_code)
            table_filter = table_filter.filter(mask) 
        elif type == 'Put':
            mask = pa.compute.equal(table_filter['BDI CODE'], put_code)
            table_filter = table_filter.filter(mask) 
        else:
            mask1 = pa.compute.equal(table_filter['BDI CODE'], call_code)
            mask2 = pa.compute.equal(table_filter['BDI CODE'], put_code)
            mask = pa.compute.or_(mask1,mask2)
            table_filter = table_filter.filter(mask) 
        
        if index_code != None:
            mask = pa.compute.starts_with(table_filter['NEG CODE'], index_code)
            table_filter = table_filter.filter(mask) 

        if code != None:
            mask = pa.compute.equal(table_filter['NEG CODE'], code)
            table_filter = table_filter.filter(mask) 

        if inplace == True:
            self.data = table_filter
            return self
        else:
            return table_filter

    def to_csv(self, data=None, path='b3_historical.csv'):
        ''' Saves current data to csv.
        ----------------------------------
        to_csv(self, data=None, path='b3_historical.csv')
        
        If data=None it will use the data in memory '''
        if data == None: data = self.data
        pa.csv.write_csv(data, path,
                 write_options=pa.csv.WriteOptions(include_header=True))
        return None

    def to_pandas(self, data=None):
        ''' Alias to transform the Table to Pandas DataFrame
        ---------------------------------
        to_pandas(self, data=None)
        '''
        if data == None: data = self.data
        return data.to_pandas()

    def line_parser(self, my_file):
        data_list = [[] for i in range(0,25)]
        for line in my_file:
            if line[0:2] == '01':
                parsed_line = [ #int(line[0:2]), #FIXED, NOT HEADER OR FOOTER == 01
                                datetime.date(int(line[2:6]),int(line[6:8]),int(line[8:10])),  #DATE OF EXCHANGE - N(08) - YYYYmmDD
                                line[10:12], #CODBDI – BDI CODE - X(02)
                                line[12:24].strip(), #CODNEG – PAPER NEGOTIATION CODE - X(12)
                                int(line[24:27]), #TPMERC – TYPE OF MARKET - N(03)
                                line[27:39].strip(), #NOMRES – ABBREVIATED NAME OF THE COMPANY THAT ISSUED THE PAPER - X(12)
                                line[39:49].strip(), #ESPECI – PAPER SPECIFICATION - X(10)
                                line[49:52].strip(), #PRAZOT – FORWARD MARKET TERM IN DAYS - X(03)
                                line[52:56].strip(), #MODREF – REFERENCE CURRENCY - X(04)
                                float(line[56:69])/100, #PREABE – MARKET PAPER OPENING FLOOR PRICE - (11)V99
                                float(line[69:82])/100, #PREMAX – MARKET PAPER HIGHEST FLOOR PRICE - (11)V99 
                                float(line[82:95])/100, #PREMIN – MARKET PAPER LOWEST FLOOR PRICE - (11)V99 
                                float(line[95:108])/100, #PREMED – MARKET PAPER AVERAGE FLOOR PRICE - (11)V99 
                                float(line[108:121])/100, #PREULT – MARKET PAPER LAST NEGOTIATED PRICE - (11)V99 
                                float(line[121:134])/100, #PREOFC – MARKET PAPER BEST PURCHASE OFFER PRICE - (11)V99 
                                float(line[134:147])/100, #PREOFV – MARKET PAPER BEST SALES OFFER PRICE - (11)V99 
                                int(line[147:152]), #TOTNEG - NEG. – NUMBER OF TRADES CONDUCTED WITH THE MARKET PAPER - N(05)
                                int(line[152:170]), #QUATOT – TOTAL QUANTITY OF TITLES TRADED WITH THIS MARKET PAPER -  N(18)
                                float(line[170:188])/100, #VOLTOT – TOTAL VOLUME OF TITLES NEGOTIATED WITH THIS MARKET PAPER - (16)V99 
                                float(line[188:201])/100, #PREEXE – STRIKE PRICE FOR THE OPTIONS MARKET OR CONTRACT AMOUNT FOR THE SECONDARY FORWARD MARKET - (11)V99 
                                int(line[201:202]), #INDOPC – STRIKE PRICE OR CONTRACT AMOUNT FOR OPTIONS OR SECONDARY FORWARD MARKETS CORRECTION INDICATOR - N(01) 
                                datetime.date(int(line[202:206]),int(line[206:208]),int(line[208:210])), #DATVEN – MATURITY DATE FOR OPTIONS OR SECONDARY FORWARD MARKETS - N(08) 
                                int(line[210:217]), #FATCOT – PAPER QUOTATION FACTOR - N(07) 
                                float(line[217:230])/1000000, #PTOEXE – STRIKE PRICE IN POINTS FOR OPTIONS REFERENCED IN US DOLLARS OR CONTRACT AMOUNT IN POINTS FOR SECONDARY FORWARD - (07)V06 
                                line[230:242], #CODISI – PAPER CODE IN THE ISIN SYSTEM OR PAPER INTERNAL CODE - X(12) 
                                int(line[242:245]), #DISMES – PAPER DISTRIBUTION NUMBER - 9(03) 
                                ]
                for i in range(0,25):
                    data_list[i].append(parsed_line[i])
        return data_list


    def data_read(self):
        source = self.source
        hist_schema = pa.schema([
            pa.field('DATE', pa.date32()), 
            pa.field('BDI CODE', pa.string()),
            pa.field('NEG CODE', pa.string()),
            pa.field('MKT TYPE', pa.int8()),
            pa.field('NAMES', pa.string()),
            pa.field('SPEC', pa.string()),
            pa.field('FORW TERM', pa.string()),
            pa.field('CURR', pa.string()),
            pa.field('OPEN', pa.float64()),
            pa.field('HIGH', pa.float64()),
            pa.field('LOW', pa.float64()),
            pa.field('AVG', pa.float64()),
            pa.field('CLOSE', pa.float64()),
            pa.field('BEST PURCHASE', pa.float64()),
            pa.field('BEST SALES', pa.float64()),
            pa.field('# NEG', pa.int32()),
            pa.field('QUANTITY', pa.int64()),
            pa.field('VOLUME', pa.float64()),
            pa.field('STK PRICE', pa.float64()),
            pa.field('STK P. CORRECTION', pa.float64()),
            pa.field('MAT DATE', pa.date32()),
            pa.field('FACTOR', pa.int32()),
            pa.field('STRIKE IN POINTS', pa.float64()),
            pa.field('ISIN CODE', pa.string()),
            pa.field('DIST #', pa.int16())])
        #data_list = [[] for i in range(0,25)]
        print(f'Loading data from {",".join(source)}.')
        tables = list()
        for each in source:
            print(f'Parsing {os.path.split(each)[-1]}.')
            if re.match(r'COTAHIST_A[1-9][0-9][0-9][0-9].zip',os.path.split(each)[-1]):
                with zipfile.ZipFile(each) as zf:
                    with io.TextIOWrapper(zf.open(zf.namelist()[0]),encoding='latin_1',errors='ignore') as my_file:
                        data_list = self.line_parser(my_file)
            else:
                with open(each, "r",encoding='latin_1',errors='ignore') as my_file:
                    data_list = self.line_parser(my_file)

            print(f'Transforming into Arrow Arrays.')
            for each in range(0,len(data_list)):
                data_list[each] = pa.array(data_list[each])
            print(f'Building Arrow Table Scheme')
            tables.append(pa.Table.from_arrays(data_list,schema=hist_schema))
        if len(tables) > 1:
            print(f'Concatenating all the tables.')
        table = pa.concat_tables(tables)
        del tables
        return table

def download_data(year,target):
    # read 1024 bytes every time 
    buffer_size = 1024
    url = f"https://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A{year}.zip"
    # download the body of response by chunk, not immediately
    response = requests.get(url, stream=True)
	# get the total file size
    file_size = int(response.headers.get("Content-Length", 0))
	# progress bar, changing the unit to bytes instead of iteration (default by tqdm)
    progress = tqdm(response.iter_content(buffer_size)
        , f"Downloading {url.split('/')[-1]} to {os.path.split(os.path.split(target)[0])[-1]}/"
        , total=file_size, unit="B"
        , unit_scale=True
        , unit_divisor=1024)
    with open(target, "wb") as f:
	    	for data in progress.iterable:
        		# write data read to the file
        		f.write(data)
        		# update the progress bar manually
        		progress.update(len(data))
    return target

def get_historical(years,target=None,delete=False):
    ''' Download historical data directly from www.b3.com.br
    get_historical(years,target=None/"directory path",delete=False/True)
    ------------------------------------------
    **years**: 
    A single year or a list of years after 1992.
    **target**(None is default): 
    Path to store the zipfiles containing data, 
    *None* will download to pwd.
    **delete**(False is default):
    Delete zipfiles at the end.'''

    # Checking if years is a list or a integer
    if isinstance(years, int):
        years = [years]
    elif not isinstance(years, list):
        raise ValueError(f'Function needs a year or list of years format YYYY, got {years}')

    #Checking if years are valid
    for each in years:
        if isinstance(each,int):
            if each < 1992:
                raise ValueError(f'This library does not supports the B3 encoding before 1992, got {each}')
            elif each > datetime.date.today().year:
                raise ValueError(f'There is no data for the future yet, got {each}')
        else:
            raise ValueError(f'Function needs a year or list of years format YYYY, got {each}')

    #Checking if path is valid
    if target == None:
        target = os.getcwd()
    else:
        if not os.path.isdir(target):
            raise ValueError(f'Target for downloading data is not a directory, got {target}')

    current_path = list() #initializing current path

    try:
        for each in years:
            to_path = os.path.join(target,f"COTAHIST_A{each}.zip")
            if os.path.isfile(to_path):
                current_path.append(to_path)
            else:
                current_path.append(download_data(each, os.path.join(target,f"COTAHIST_A{each}.zip")))
    except:
        raise ValueError(f"Could not download data for year: {each}.")

    data = b3_historical(current_path)

    if delete:
        for file_path in years:
            if os.path.exists(file_path):
                os.remove(file_path)
    
    return data


