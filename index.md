# B3 Historical Quotes - Options parser

A simple parser for historical quotes available at Brazil's stock exchange B3/Bovespa.

It can download quotes by year without the need of a captcha. [B3 - Historical Quotes](https://www.b3.com.br/en_us/market-data-and-indices/data-services/market-data/historical-data/equities/historical-quote-data/).
>b3_historical.get_historical([list of years])

The site checks for a captcha but doesn't generates a session token so you can go directly to the path, which is *"https://bvmf.bmfbovespa.com.br/InstDados/SerHist/COTAHIST_A{year}.zip"*.

**Parsed options file for 2000 and index options file from 1992 to 2021 are already prepared, just click [download](../download.html) on the navigation bar.**

Options file for the whole range is a 642 MB file, being 184 MB only for *2021*, so I would need more storage. 

Learn more about the project in the **[ReadMe](../README.html)** section. 

## Author
I am Leonardo Brioschi, a Ph.D. Student at Fucape Business School, visit my site at [leobrioschi.gitlab.io](https://leobrioschi.gitlab.io)

## License
For this project, the UNLICENSE was the choice. Learn more in the repo.
