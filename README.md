# [B3 Historical Quotes - Options parser](https://leobrioschi.gitlab.io/b3-historical-quotes-options-parser)

A simple parser for historical quotes available at Brazil's stock exchange B3, but only after 1992 as data prior has strange encoding. This parser is meant to only keep Options data or specific data, since it is hard to fetch it from existing data providers (such as *yfinance* and *Alpha Vantage*)
As 06/2022 the raw data is available at [B3 - Historical Quotes](https://www.b3.com.br/en_us/market-data-and-indices/data-services/market-data/historical-data/equities/historical-quote-data/).

It was developed with **Apache Arrow** Array and Table structure, since the whole raw data can be a bit rough on some computers, as mine.

With some filter in place, the advice is to transform to a pandas DataFrame and keep your analysis.

## Getting started

1. **Get data:**
    - Pass a list of years to *b3_historical.get_historical()*, **or**;
    - **Get the raw data** at [B3 - Historical Quotes](https://www.b3.com.br/en_us/market-data-and-indices/data-services/market-data/historical-data/equities/historical-quote-data/).
2. **Load the implemented class on b3_historical.py**
>from b3_historical import b3_historical
3. **Create an instance with the source_path to the TXT file**
>historical_data = b3_historical('source_path.txt')
4. **Use any filter or not, then save to csv or transform into a pandas DF**
>Calls = historical.options(type='Call')
>Calls.to_pandas()

## Installation
Copy **b3_historical.py** to your working directory so you can import the class

## Usage
Please look for the **example.ipynb** for a notebook explaining how to use, or GOTO *Getting Started*.

## Roadmap
I am not sure if I am going to implement more filters, since the correct **BDI CODE** can yield a third of the data, which is already pretty manageable in any computer by *pandas*

## Contributing
If you want to contribute, simple send a message/e-mail. I would like some collaboration to expand, or any invitation to contribute to another exting project.

## Author
I am Leonardo Brioschi, a Ph.D. Student at Fucape Business School, visit my site at [leobrioschi.gitlab.io](https://leobrioschi.gitlab.io)

## License
For this project, the UNLICENSE was the choice. Please read more at *LICENSE*

## Project status
Ongoing