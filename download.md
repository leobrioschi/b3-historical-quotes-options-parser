# Download files

Two files currently are presented at the repo:

1. **Options daily quotes for 2000:**
    - *[options_2000.csv(1.8 mb)](https://gitlab.com/leobrioschi/b3-historical-quotes-options-parser/-/raw/main/options_2000.csv?inline=false)*
2. **Index Options daily quotes from 1992 to 2021:**
    - *[index_options.csv(25 mb)](https://gitlab.com/leobrioschi/b3-historical-quotes-options-parser/-/raw/main/options_2021.csv?inline=false)*
